import java.io.File;
import java.io.FileWriter;
import java.io.PrintWriter;

public class FileTest {

    public static void main(String[] args) {
        try {
            File file = new File("d:/docker", "Dockerfile");
            if (!file.getParentFile().exists()) {
                file.getParentFile().mkdirs();
            }

            file.createNewFile();
            FileWriter fw = new FileWriter(file);
            PrintWriter pw = new PrintWriter(fw);
            pw.println("FROM java");
            pw.println("ADD /app.jar");
            pw.flush();
            fw.flush();
            pw.close();
            fw.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
