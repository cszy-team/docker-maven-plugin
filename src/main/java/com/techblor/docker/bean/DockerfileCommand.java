package com.techblor.docker.bean;

import java.util.ArrayList;
import java.util.List;

/**
 * dockerfile的指令集
 */
public class DockerfileCommand {

    /**
     * 指令集
     */
    private List<String> command;

    public DockerfileCommand(List<String> command) {
        this.command = command;
    }

    public List<String> getCommand() {
        return command;
    }

    public void setCommand(List<String> command) {
        this.command = command;
    }

    public static DockerfileCommandBuilder builder() {
        return new DockerfileCommandBuilder();
    }

    public static class DockerfileCommandBuilder {

        private List<String> command = new ArrayList<>();

        public DockerfileCommandBuilder addCommand(String command, String value) {
            String dockerfileCommand = String.format("%s %s", command, value);
            this.command.add(dockerfileCommand);
            return this;
        }

        public DockerfileCommand build() {
            return new DockerfileCommand(command);
        }
    }

}