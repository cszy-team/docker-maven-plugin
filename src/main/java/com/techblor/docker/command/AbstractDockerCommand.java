//package com.techblor.docker.command;
//
//import java.io.InputStreamReader;
//import java.io.LineNumberReader;
//import java.util.ArrayList;
//import java.util.List;
//
///**
// * 抽象docker命令方法
// */
//public abstract class AbstractDockerCommand implements DockerCommand {
//
//    public abstract List<String> buildCommand();
//
//    /**
//     * 执行命令
//     */
//    public void executeCommand(String command) {
//        try {
//            ProcessBuilder processBuilder = new ProcessBuilder(command);
//            processBuilder.redirectErrorStream(true);
//            // 读取命令的输出信息
//            List<String> logs = new ArrayList<>();
//            Process process = processBuilder.start();
//            InputStreamReader ir = new InputStreamReader(process.getInputStream());
//            LineNumberReader input = new LineNumberReader(ir);
//            String line;
//            process.waitFor();
//            if (process.exitValue() != 0) {
//                // 说明命令执行失败
//                // 可以进入到错误处理步骤中
//                return;
//            }
//
//            while ((line = input.readLine()) != null) {
//                logs.add(line);
//            }
//
//            logs.forEach(System.out::println);
//        } catch (Exception e) {
//            System.out.println("构建镜像失败:" + e);
//        }
//    }
//}
