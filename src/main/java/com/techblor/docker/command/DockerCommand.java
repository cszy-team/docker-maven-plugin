package com.techblor.docker.command;

import java.util.List;

/**
 * docker命令接口
 */
public interface DockerCommand {

    /**
     * 构建命令
     *
     * @return
     */
    List<String> buildCommand();

    /**
     * 执行命令
     */
    void executeCommand(List<String> command);
}
