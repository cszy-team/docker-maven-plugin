package com.techblor.docker.config;

import com.alibaba.fastjson.JSON;
import org.apache.maven.plugins.annotations.Parameter;

/**
 * 目录配置
 */
public class DirectoryConfig {

    /**
     * dockerfile路径
     */
    @Parameter(property = "dockerfile")
    private String dockerfile;

    /**
     * 项目路径
     */
    @Parameter(property = "projectPath")
    private String projectPath;

    /**
     * 文件名称
     */
    @Parameter(property = "projectFile")
    private String projectFile;

    /**
     * 目标路径
     */
    @Parameter(property = "dockerfile")
    private String targetPath;

    public String getDockerfile() {
        return dockerfile;
    }

    public void setDockerfile(String dockerfile) {
        this.dockerfile = dockerfile;
    }

    public String getProjectPath() {
        return projectPath;
    }

    public void setProjectPath(String projectPath) {
        this.projectPath = projectPath;
    }

    public String getProjectFile() {
        return projectFile;
    }

    public void setProjectFile(String projectFile) {
        this.projectFile = projectFile;
    }

    public String getTargetPath() {
        return targetPath;
    }

    public void setTargetPath(String targetPath) {
        this.targetPath = targetPath;
    }

    @Override
    public String toString() {
        return JSON.toJSONString(this);
    }
}
