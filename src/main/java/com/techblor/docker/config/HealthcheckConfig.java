package com.techblor.docker.config;

import com.alibaba.fastjson.JSON;

public class HealthcheckConfig {

    private String interval;

    private String timeout;

    private Integer retries;

    private String cmd;

    public String getInterval() {
        return interval;
    }

    public void setInterval(String interval) {
        this.interval = interval;
    }

    public String getTimeout() {
        return timeout;
    }

    public void setTimeout(String timeout) {
        this.timeout = timeout;
    }

    public Integer getRetries() {
        return retries;
    }

    public void setRetries(Integer retries) {
        this.retries = retries;
    }

    public String getCmd() {
        return cmd;
    }

    public void setCmd(String cmd) {
        this.cmd = cmd;
    }

    @Override
    public String toString() {
        return JSON.toJSONString(this);
    }
}
