package com.techblor.docker.config;

import com.alibaba.fastjson.JSON;
import org.apache.maven.plugins.annotations.Parameter;

/**
 * docker配置
 */
public class DockerConfig {

    /**
     * 远程docker的host连接地址: 如: tcp://127.0.0.1:2375
     */
    @Parameter(property = "host")
    private String host;

    /**
     * docker证书路径
     */
    @Parameter(property = "certPath")
    private String certPath;

    /**
     * docker镜像仓库网址
     */
    @Parameter(property = "registry")
    private String registry;

    /**
     * docker镜像仓库登录账号
     */
    @Parameter(property = "username")
    private String username;

    /**
     * docker镜像仓库登录密码
     */
    @Parameter(property = "password")
    private String password;

    /**
     * maven settings.xml server配置的id
     */
    @Parameter(property = "serverId")
    private String serverId;

    public String getHost() {
        return host;
    }

    public void setHost(String host) {
        this.host = host;
    }

    public String getRegistry() {
        return registry;
    }

    public void setRegistry(String registry) {
        this.registry = registry;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getServerId() {
        return serverId;
    }

    public void setServerId(String serverId) {
        this.serverId = serverId;
    }

    @Override
    public String toString() {
        return JSON.toJSONString(this);
    }
}
