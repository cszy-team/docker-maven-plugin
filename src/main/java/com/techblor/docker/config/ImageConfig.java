package com.techblor.docker.config;

import com.alibaba.fastjson.JSON;
import org.apache.maven.plugins.annotations.Parameter;

import java.util.List;
import java.util.Map;

/**
 * 镜像配置
 */
public class ImageConfig {

    /**
     * 镜像名称
     */
    @Parameter(property = "name", required = true)
    private String name;

    /**
     * 别名
     */
    @Parameter(property = "alias")
    private String alias;

    /**
     * 镜像版本号: 如果未设定则以latest为版本号
     */
    @Parameter(property = "tags")
    private List<String> tags;

    /**
     * 基础镜像
     */
    @Parameter(property = "from", required = true)
    private String from;

    /**
     * 执行命令行参数: RUN 是在 docker build 时运行
     */
    @Parameter(property = "run")
    private List<String> run;

    /**
     * 复制指令参数
     */
    @Parameter(property = "copy")
    private List<String> copy;

    /**
     * 添加复制指令参数
     */
    @Parameter(property = "add")
    private List<String> add;

    /**
     * 运行程序指令参数: CMD 在docker run 时运行
     */
    @Parameter(property = "cmd")
    private List<String> cmd;

    /**
     * 运行程序指令
     */
    @Parameter(property = "entryPoint", required = true)
    private String entryPoint;

    /**
     * 环境变量
     */
    @Parameter(property = "env")
    private Map<String, String> env;

    /**
     * 构建参数
     */
    @Parameter(property = "arg")
    private Map<String, String> arg;

    /**
     * 匿名数据卷
     */
    @Parameter(property = "volume")
    private List<String> volume;

    /**
     * 声明端口
     */
    @Parameter(property = "expose")
    private List<String> expose;

    /**
     * 用于指定工作目录
     */
    @Parameter(property = "workdir")
    private String workdir;

    /**
     * 用于指定执行后续命令的用户和用户组
     */
    @Parameter(property = "user")
    private String user;

    /**
     * 用于指定某个程序或者指令来监控 docker 容器服务的运行状态
     */
    @Parameter(property = "healthcheck")
    private HealthcheckConfig healthcheck;

    /**
     * 用于给镜像添加一些元数据
     */
    @Parameter(property = "label")
    private List<String> label;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAlias() {
        return alias;
    }

    public void setAlias(String alias) {
        this.alias = alias;
    }

    public List<String> getTags() {
        return tags;
    }

    public void setTags(List<String> tags) {
        this.tags = tags;
    }

    public String getFrom() {
        return from;
    }

    public void setFrom(String from) {
        this.from = from;
    }

    public List<String> getRun() {
        return run;
    }

    public void setRun(List<String> run) {
        this.run = run;
    }

    public List<String> getCopy() {
        return copy;
    }

    public void setCopy(List<String> copy) {
        this.copy = copy;
    }

    public List<String> getAdd() {
        return add;
    }

    public void setAdd(List<String> add) {
        this.add = add;
    }

    public List<String> getCmd() {
        return cmd;
    }

    public void setCmd(List<String> cmd) {
        this.cmd = cmd;
    }

    public String getEntryPoint() {
        return entryPoint;
    }

    public void setEntryPoint(String entryPoint) {
        this.entryPoint = entryPoint;
    }

    public Map<String, String> getEnv() {
        return env;
    }

    public void setEnv(Map<String, String> env) {
        this.env = env;
    }

    public Map<String, String> getArg() {
        return arg;
    }

    public void setArg(Map<String, String> arg) {
        this.arg = arg;
    }

    public List<String> getVolume() {
        return volume;
    }

    public void setVolume(List<String> volume) {
        this.volume = volume;
    }

    public List<String> getExpose() {
        return expose;
    }

    public void setExpose(List<String> expose) {
        this.expose = expose;
    }

    public String getWorkdir() {
        return workdir;
    }

    public void setWorkdir(String workdir) {
        this.workdir = workdir;
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public HealthcheckConfig getHealthcheck() {
        return healthcheck;
    }

    public void setHealthcheck(HealthcheckConfig healthcheck) {
        this.healthcheck = healthcheck;
    }

    public List<String> getLabel() {
        return label;
    }

    public void setLabel(List<String> label) {
        this.label = label;
    }

    @Override
    public String toString() {
        return JSON.toJSONString(this);
    }
}
