package com.techblor.docker.config;

import java.util.List;

public class CommandConfig {

    private List<String> command;

    public List<String> getCommand() {
        return command;
    }

    public void setCommand(List<String> command) {
        this.command = command;
    }
}
