package com.techblor.docker.util;

import java.util.Properties;

/**
 * 分隔符工具类
 */
public class SeparatorUtil {

    /**
     * 获取分隔符的系统属性
     */
    private static final Properties PROPERTIES = new Properties(System.getProperties());

    /**
     * 获取当前平台上的行分隔符
     *
     * @return line separator
     */
    public static String getLineSeparator() {
        return PROPERTIES.getProperty("line.separator");
    }

    /**
     * 获取当前平台上的路径分隔符
     *
     * @return path separator
     */
    public static String getPathSeparator() {
        return PROPERTIES.getProperty("path.separator");
    }
}
