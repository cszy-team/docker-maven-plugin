package com.techblor.docker.util;

import java.util.Iterator;
import java.util.List;

public class JoinerUtil {

    /**
     * 返回将字符串集合转换成以逗号隔开的字符串数据
     *
     * @param list
     * @param separator
     * @return
     */
    public static String join(List<String> list, String separator) {
        if(list == null || list.isEmpty()){
            return null;
        }

        Iterator<?> iterator = list.iterator();
        if (!iterator.hasNext()) {
            return null;
        }

        Object first = iterator.next();
        if (!iterator.hasNext()) {
            return first.toString();
        } else {
            StringBuilder sb = new StringBuilder(256);
            if (first != null) {
                sb.append(first);
            }

            while (iterator.hasNext()) {
                sb.append(separator);
                Object next = iterator.next();
                if (next != null) {
                    sb.append(next);
                }
            }

            return sb.toString();
        }
    }
}
