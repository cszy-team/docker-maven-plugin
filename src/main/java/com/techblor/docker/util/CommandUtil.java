package com.techblor.docker.util;

import java.io.IOException;

/**
 * 命令行工具类
 */
public class CommandUtil {

    /**
     * 执行命令
     *
     * @param command 命令
     * @return
     * @throws IOException
     */
    public static Process exec(String command) throws IOException {
        return Runtime.getRuntime().exec(command);
    }
}
