package com.techblor.docker.util;

/**
 * 系统工具类
 */
public class SystemUtil {

    private static final String os = System.getProperty("os.name").toLowerCase();

    /**
     * 获取操作系统
     *
     * @return
     */
    public static String getOperatingSystem() {
        return os;
    }
}
