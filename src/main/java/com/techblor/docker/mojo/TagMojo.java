package com.techblor.docker.mojo;

import org.apache.maven.plugin.AbstractMojo;
import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugin.MojoFailureException;
import org.apache.maven.plugins.annotations.Mojo;

@Mojo(name = "tag", threadSafe = true)
public class TagMojo extends AbstractMojo {

    public void execute() throws MojoExecutionException, MojoFailureException {
    }

}
