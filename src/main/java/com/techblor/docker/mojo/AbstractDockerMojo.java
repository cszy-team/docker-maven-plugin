package com.techblor.docker.mojo;

import com.techblor.docker.command.DockerCommand;
import com.techblor.docker.config.DirectoryConfig;
import com.techblor.docker.config.DockerConfig;
import com.techblor.docker.config.ImageConfig;
import com.techblor.docker.util.SystemUtil;
import org.apache.maven.plugin.AbstractMojo;
import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugin.MojoFailureException;
import org.apache.maven.plugins.annotations.Parameter;

public abstract class AbstractDockerMojo extends AbstractMojo implements DockerCommand {

//    @Component(role = MavenSession.class)
//    protected MavenSession session;
//
//    @Component(role = MojoExecution.class)
//    protected MojoExecution execution;
//
//    @Component
//    private Settings settings;
//
//    @Component(role = SecDispatcher.class, hint = "mng-4384")
//    private SecDispatcher secDispatcher;

    /**
     * docker配置
     */
    @Parameter(property = "docker", required = true)
    protected DockerConfig docker;

    /**
     * 镜像配置
     */
    @Parameter(property = "image", required = true)
    protected ImageConfig image;

    /**
     * 目录配置
     */
    @Parameter(property = "directory", required = true)
    protected DirectoryConfig directory;

    @Override
    public String toString() {
        return "{" +
                "docker:" + docker +
                ", image:" + image +
                ", directory:" + directory +
                '}';
    }

    public void execute() throws MojoExecutionException, MojoFailureException {
        String operatingSystem = SystemUtil.getOperatingSystem();
        getLog().info("操作系统: " + operatingSystem);
        System.out.println(this);
        executeCommand(buildCommand());
    }


}
